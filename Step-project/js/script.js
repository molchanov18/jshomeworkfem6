/************* Our Services *************/

const serviceTabs = document.querySelector(".services-tabs");
const servicesTabsContent = document.querySelector(".services-tabs-content");
const serviceTabsChildred = serviceTabs.children;
const servicesTabsContentChildren = servicesTabsContent.children;

for(let i = 0; i < serviceTabsChildred.length; i++){
    serviceTabsChildred[i].dataset.index = String(i);   
    if(i !== 0){
        servicesTabsContentChildren[i].hidden = true;
    }
}

serviceTabs.addEventListener("click", () => {
    serviceTabs.querySelector(".active").classList.remove("active");
    servicesTabsContent.querySelector("li:not([hidden])").hidden = true;
    event.target.classList.add("active");
    servicesTabsContentChildren[event.target.dataset.index].hidden = false;
})

/************* Our Amazing Work *************/

const filterTabs = document.querySelector('.our-amazing-work-tabs');
const filterImg = document.querySelectorAll('.amazing-image-container')
const loadBtn = document.querySelector('.load-imitate')
console.log(loadBtn)

window.addEventListener("DOMContentLoaded", () => {
    removeAllItems(filterImg);
    showItems(filterImg, 12);
})

loadBtn.addEventListener("click", () => {
    showItems(filterImg, 12);
    hideBtn(filterImg, loadBtn);
})

filterTabs.addEventListener("click", (e) => { 
    e.target.closest("ul").querySelectorAll("li").forEach(item => item.classList.remove("active-tab"))
    e.target.classList.add("active-tab")
    console.log(e.target);
    // if(e.target.dataset.category === "all" && e.target.classList.contains("active-tab")) {
    //     console.log("first")
    //     return;
    // }
    if (e.target.dataset.category === "all") {
        console.log("second");
        removeAllItems(filterImg);
        showItems(filterImg, 12);
    }
    else {
        showItemsByCat(filterImg, e.target.dataset.category)
    }
})

function removeAllItems (items) {
    items.forEach(item => {
        item.classList.add("hidden")
    })
}

function showItems (items, count) {
    const hiddenItems = [...items].filter(item => item.classList.contains("hidden")).slice(0, count)
    hiddenItems.forEach(item => item.classList.remove("hidden"))  
}

function hideBtn (items, button) {
    const showedItems = [...items].filter(item => item.classList.contains("hidden"));
    if(!showedItems.length) {
        button.classList.add("hidden")
    }
}

function showItemsByCat (items, category) {
        removeAllItems(items);
        const imgToShow = [...items].filter(item => item.dataset.category === category);
        imgToShow.forEach(item => item.classList.remove("hidden"));
}   
