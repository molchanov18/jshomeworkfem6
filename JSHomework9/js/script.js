const tabs = document.querySelector(".tabs"); // находим элемент с классом tabs
const tabsContent = document.querySelector(".tabs-content"); // находим элемент с классом tabs-content
const tabsChildren = tabs.children; // получаем массив из детей переменной tabs(li)
const tabsContentChildren = tabsContent.children; //получаем массив из детей переменной tabsContent

for(let i = 0; i < tabsChildren.length; i++){   // запускаем цикл который перебирает элементы массива tabsChildren
    tabsChildren[i].dataset.index = String(i);  // обращаемся в массив по счетчику i. И присваеваем data атрибуту index счетчик i
    
    if(i !== 0){
        tabsContentChildren[i].hidden = true;   // Скрываем текст из tabsContentChildren
    }
}

tabs.addEventListener("click", () => {          // присваеваем всем tabs слушатель событий по клику. 
    tabs.querySelector(".active").classList.remove("active")    // Находим элемент у которого есть класс active и убераем его
    tabsContent.querySelector("li:not([hidden])").hidden = true;    // Ноходим текст который не спрятан и прячем его.
    event.target.classList.add("active")    // При выполнении события присваеваем элементу на котором было выполнено действие класс active
    tabsContentChildren[event.target.dataset.index].hidden = false; // Стучимся в массив тектов по дата атрибуту элемента по которому мы выполнили действие.
})



